# GlycanCompositionConverter

Java project for converting glycan composition to WURCS, as well as GlycoCT.

## Text format for composition
Composition in this program can be specified as a text format.

For example, when the composition contains three hexoses and two N-acetyl hexoses,
it can be represented as following:  
`Hex:3|HexNAc:2`

Substituents connecting to a monosaccharide but which is unknown can also be represented.

For example, the composition, which contains three hexoses, two N-acetyl hexoses
 and one acetyl group connecting to one of the monosaccharides,
 can be represented as following:  
`Hex:3|HexNAc:2|Ac:1`

## Default residue types
Some monosaccharides and substituents can be used as default.

The default monosaccharides and substituents need to load from default dictionary,
using methods `MonosaccharideDictionary.loadDefaultDictionary()` and 
`SubstituentDictionary.loadDefaultDictionary()`, respectively, before use the residues.

### Monosaccharides
| Name | Description |
| :---: | :--- |
| Hex | Hexopyranose |
| HexNAc | N-acetylhexopyranose |
| dHex | Deoxy-hexopyranose |
| Glc | D-glucopyranose |
| Gal | D-galactopyranose |
| Man | D-mannopyranose |
| Fuc | L-fucopyranose |
| GlcNAc | N-acetyl-D-glucohexopyranose |
| Neu5Ac | N-acetyl-neuraminic acid |

or the other monosaccharides which can be represented as symbols in SNFG notation

### Substituents
| Name | Description |
| :---: | :--- |
| Ac | O-acetyl group |
| Me | O-methyl group |
| P | Phosphate |
| S | Sulfate |

## Sample codes

### To convert composition with text format to WURCS

```java
import org.glycoinfo.GlycanCompositionConverter.conversion.CompositionConverter;
import org.glycoinfo.GlycanCompositionConverter.structure.Composition;
import org.glycoinfo.GlycanCompositionConverter.utils.CompositionUtils;

public class TestCompositionParser {

	public static void main(String[] args) throws Exception {
		String strComposition = "Hex:3|HexNAc:2";

		Composition compo = CompositionUtils.parse(strComposition);
		String strWURCS = CompositionConverter.toWURCS(compo);

		System.out.println(strWURCS);
		// WURCS=2.0/2,5,4/[axxxxh-1x_1-5_2*NCC/3=O][axxxxh-1x_1-5]/1-1-2-2-2/a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?
	}

}
```

## Examples

Composition:  
`Hex:3|HexNAc:2`

WURCS:  
`WURCS=2.0/2,5,4/[axxxxh-1x_1-5_2*NCC/3=O][axxxxh-1x_1-5]/1-1-2-2-2/a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?_a?|b?|c?|d?|e?}-{a?|b?|c?|d?|e?`

Composition:  
`Hex:3|HexNAc:2|dHex:1`

WURCS:  
`WURCS=2.0/3,6,5/[axxxxm-1x_1-5][axxxxh-1x_1-5_2*NCC/3=O][axxxxh-1x_1-5]/1-2-2-3-3-3/a?|b?|c?|d?|e?|f?}-{a?|b?|c?|d?|e?|f?_a?|b?|c?|d?|e?|f?}-{a?|b?|c?|d?|e?|f?_a?|b?|c?|d?|e?|f?}-{a?|b?|c?|d?|e?|f?_a?|b?|c?|d?|e?|f?}-{a?|b?|c?|d?|e?|f?_a?|b?|c?|d?|e?|f?}-{a?|b?|c?|d?|e?|f?`

Composition:  
`Fuc:1|GlcNAc:2|Man:3`

WURCS:  
`WURCS=2.0/3,6,5/[a1221m-1x_1-5][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5]/1-2-2-3-3-3/a?|b?|c?|d?|e?|f?}-{a?|b?|c?|d?|e?|f?_a?|b?|c?|d?|e?|f?}-{a?|b?|c?|d?|e?|f?_a?|b?|c?|d?|e?|f?}-{a?|b?|c?|d?|e?|f?_a?|b?|c?|d?|e?|f?}-{a?|b?|c?|d?|e?|f?_a?|b?|c?|d?|e?|f?}-{a?|b?|c?|d?|e?|f?`


Composition:
`Gal:1|Glc:1|Neu5Ac:1`

WURCS:
`WURCS=2.0/3,3,2/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5][a2112h-1x_1-5]/1-2-3/a?|b?|c?}-{a?|b?|c?_a?|b?|c?}-{a?|b?|c?`

Comopsition:
`Gal:1|Glc:1|Neu5Ac:1|Ac:1`

WURCS:
`WURCS=2.0/3,3,3/[Aad21122h-2x_2-6_5*NCC/3=O][a2122h-1x_1-5][a2112h-1x_1-5]/1-2-3/a?|b?|c?}*OCC/3=O_a?|b?|c?}-{a?|b?|c?_a?|b?|c?}-{a?|b?|c?`

