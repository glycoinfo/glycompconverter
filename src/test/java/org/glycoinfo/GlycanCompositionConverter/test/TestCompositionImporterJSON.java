package org.glycoinfo.GlycanCompositionConverter.test;

import java.util.List;

import org.glycoinfo.GlycanCompositionConverter.io.CompositionImporter;
import org.glycoinfo.GlycanCompositionConverter.structure.Composition;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MonosaccharideDictionary;
import org.glycoinfo.GlycanCompositionConverter.utils.TextFileUtils;

public class TestCompositionImporterJSON {

	public static void main(String[] args) throws Exception {
		String json = "";
		for ( String line : TextFileUtils.getLines("/test_input.json") )
			json += line;

		// Failed due to no current dictionary
		List<Composition> lComps = CompositionImporter.parseJsonArray(json);

		MonosaccharideDictionary.loadDefaultDictionaries();

		lComps = CompositionImporter.parseJsonArray(json);
		for ( Composition comp : lComps ) {
			System.out.println(comp);
		}
	}

}
