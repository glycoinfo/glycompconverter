package org.glycoinfo.GlycanCompositionConverter.test;

import org.glycoinfo.GlycanCompositionConverter.conversion.CompositionConverter;
import org.glycoinfo.GlycanCompositionConverter.structure.Composition;
import org.glycoinfo.GlycanCompositionConverter.utils.CompositionUtils;

public class TestCompositionConverter {

	public static void main(String[] args) throws Exception {
		for ( String strCompo : args ) {
			Composition compo = CompositionUtils.parse(strCompo);
			String strWURCS = CompositionConverter.toWURCS(compo);
			System.out.println(strWURCS);
		}
	}

}
