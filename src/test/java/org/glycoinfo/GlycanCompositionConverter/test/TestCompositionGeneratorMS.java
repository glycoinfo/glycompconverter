package org.glycoinfo.GlycanCompositionConverter.test;

import org.eurocarbdb.MolecularFramework.io.GlycoCT.SugarExporterGlycoCTCondensed;
import org.glycoinfo.GlycanCompositionConverter.CompositionGeneratorSugar;
import org.glycoinfo.GlycanCompositionConverter.structure.Composition;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MSType;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MonosaccharideDictionary;
import org.glycoinfo.GlycanFormatconverter.util.exchange.SugarToWURCSGraph.SugarToWURCSGraph;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;

public class TestCompositionGeneratorMS {

	public static void main(String[] args) throws Exception {
		MonosaccharideDictionary.printCurrentMonosaccharideDictionaries();

		MonosaccharideDictionary.loadDefaultDictionaries();
		MonosaccharideDictionary.printCurrentMonosaccharideDictionaries();

		for ( MSType type : MonosaccharideDictionary.getMonosaccharides() ) {
			Composition compo = new Composition();
			compo.addMonosaccharide(type);
			testConversion(compo);
		}
	}

	private static void testConversion(Composition compo) throws Exception {
		System.out.println(compo);
		System.out.println();

		CompositionGeneratorSugar t_oSugarGen = new CompositionGeneratorSugar();
		SugarExporterGlycoCTCondensed exporter = new SugarExporterGlycoCTCondensed();
		
		t_oSugarGen.start(compo);
		exporter.start(t_oSugarGen.getSugar());
		System.out.println( exporter.getHashCode() );

		SugarToWURCSGraph t_s2w = new SugarToWURCSGraph();
		t_s2w.start(t_oSugarGen.getSugar());

		WURCSFactory factory = new WURCSFactory(t_s2w.getGraph());
		System.out.println(factory.getWURCS());
		System.out.println();
	}
}
