package org.glycoinfo.GlycanCompositionConverter.test;

import org.eurocarbdb.MolecularFramework.io.GlycoCT.SugarExporterGlycoCTCondensed;
import org.glycoinfo.GlycanCompositionConverter.CompositionGeneratorSugar;
import org.glycoinfo.GlycanCompositionConverter.structure.Composition;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MSType;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MonosaccharideDictionary;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.SubstituentDictionary;
import org.glycoinfo.GlycanFormatconverter.util.exchange.SugarToWURCSGraph.SugarToWURCSGraph;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;

public class TestCompositionGeneratorSugar {

	public static void main(String[] args) throws Exception {
		MonosaccharideDictionary.printCurrentMonosaccharideDictionaries();

		MonosaccharideDictionary.loadDefaultDictionaries();
		for ( MSType type : MonosaccharideDictionary.getMonosaccharides() ) {
			System.out.println(type);
		}
		MonosaccharideDictionary.printCurrentMonosaccharideDictionaries();

		Composition compo = new Composition();
		compo.addMonosaccharide(MonosaccharideDictionary.getMonosaccharideType("Hex"), 3);
		compo.addMonosaccharide(MonosaccharideDictionary.getMonosaccharideType("HexNAc"), 2);
		testConversion(compo);

		compo = new Composition();
		compo.addMonosaccharide(MonosaccharideDictionary.getMonosaccharideType("Man"), 3);
		compo.addMonosaccharide(MonosaccharideDictionary.getMonosaccharideType("GlcNAc"), 2);
		testConversion(compo);

		compo.addMonosaccharide(MonosaccharideDictionary.getMonosaccharideType("GlcNAc"), 2);
		compo.addMonosaccharide(MonosaccharideDictionary.getMonosaccharideType("Gal"), 2);
		compo.addMonosaccharide(MonosaccharideDictionary.getMonosaccharideType("Neu5Ac"), 2);
		testConversion(compo);

		compo.addMonosaccharide(MonosaccharideDictionary.getMonosaccharideType("GlcNAc"), 2);
		compo.addMonosaccharide(MonosaccharideDictionary.getMonosaccharideType("Gal"), 2);
		compo.addMonosaccharide(MonosaccharideDictionary.getMonosaccharideType("Neu5Ac"), 2);
		testConversion(compo);

		SubstituentDictionary.loadDefaultDictionaries();

		compo.addSubstituent(SubstituentDictionary.getSubstituentType("Ac"), 2);
		testConversion(compo);
	}

	private static void testConversion(Composition compo) throws Exception {
		System.out.println(compo);
		System.out.println();

		CompositionGeneratorSugar t_oSugarGen = new CompositionGeneratorSugar();
		SugarExporterGlycoCTCondensed exporter = new SugarExporterGlycoCTCondensed();
		
		t_oSugarGen.start(compo);
		exporter.start(t_oSugarGen.getSugar());
		System.out.println( exporter.getHashCode() );

		SugarToWURCSGraph t_s2w = new SugarToWURCSGraph();
		t_s2w.start(t_oSugarGen.getSugar());

		WURCSFactory factory = new WURCSFactory(t_s2w.getGraph());
		System.out.println(factory.getWURCS());
		System.out.println();
	}
}
