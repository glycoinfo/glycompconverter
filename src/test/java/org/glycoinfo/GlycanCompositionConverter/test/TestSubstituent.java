package org.glycoinfo.GlycanCompositionConverter.test;

import org.glycoinfo.GlycanCompositionConverter.structure.Composition;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.SubstType;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.SubstituentDictionary;

public class TestSubstituent {

	public static void main(String[] args) throws Exception {
		SubstituentDictionary.loadDefaultDictionaries();

		SubstituentDictionary.findSubstituentType("Ac");
		for ( SubstType type : SubstituentDictionary.getSubstituents() ) {
			System.out.println(type.getSubstituentType());
		}

		Composition compo = new Composition();
		compo.addSubstituent(SubstituentDictionary.getSubstituentType("Ac"), 2);
		System.out.println(compo);
	}

}
