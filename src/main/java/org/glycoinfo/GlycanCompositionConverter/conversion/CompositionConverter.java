package org.glycoinfo.GlycanCompositionConverter.conversion;

import org.eurocarbdb.MolecularFramework.io.GlycoCT.SugarExporterGlycoCTCondensed;
import org.eurocarbdb.MolecularFramework.sugar.GlycoconjugateException;
import org.eurocarbdb.MolecularFramework.sugar.Sugar;
import org.eurocarbdb.MolecularFramework.util.visitor.GlycoVisitorException;
import org.glycoinfo.GlycanCompositionConverter.CompositionGeneratorSugar;
import org.glycoinfo.GlycanCompositionConverter.structure.Composition;
import org.glycoinfo.GlycanFormatconverter.util.exchange.SugarToWURCSGraph.SugarToWURCSGraph;
import org.glycoinfo.WURCSFramework.util.WURCSException;
import org.glycoinfo.WURCSFramework.util.WURCSFactory;
import org.glycoinfo.WURCSFramework.util.exchange.WURCSExchangeException;

public class CompositionConverter {

	public static String toWURCS(Composition compo) throws ConversionException {
		SugarToWURCSGraph t_s2w = new SugarToWURCSGraph();
		try {
			t_s2w.start(toSugar(compo));
		} catch (WURCSExchangeException e) {
			throw new ConversionException("Error in converting Sugar object.", e);
		}

		String strWURCS = null;
		try {
			WURCSFactory factory = new WURCSFactory(t_s2w.getGraph());
			strWURCS = factory.getWURCS();
		} catch (WURCSException e) {
			throw new ConversionException("Error in encoding WURCS.", e);
		}

		return strWURCS;
	}

	public static String toGlycoCT(Composition compo) throws ConversionException {
		SugarExporterGlycoCTCondensed exporter = new SugarExporterGlycoCTCondensed();

		try {
			exporter.start(toSugar(compo));
		} catch (GlycoVisitorException e) {
			throw new ConversionException("Error in generating GlycoCT code.", e);
		}

		return exporter.getHashCode();
	}

	public static Sugar toSugar(Composition compo) throws ConversionException {
		CompositionGeneratorSugar t_oSugarGen = new CompositionGeneratorSugar();

		try {
			t_oSugarGen.start(compo);
		} catch (GlycoconjugateException e) {
			throw new ConversionException("Error in generating Sugar.", e);
		}

		return t_oSugarGen.getSugar();
	}
}
