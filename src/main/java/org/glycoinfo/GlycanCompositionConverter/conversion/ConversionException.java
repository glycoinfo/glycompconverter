package org.glycoinfo.GlycanCompositionConverter.conversion;

public class ConversionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -251346252501813452L;

	public ConversionException(String message) {
		super(message);
	}

	public ConversionException(String message, Throwable t) {
		super(message, t);
	}
}
