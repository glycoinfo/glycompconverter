package org.glycoinfo.GlycanCompositionConverter.io;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JSONLoader {

	private static final Logger logger = LoggerFactory.getLogger(JSONLoader.class);

	private Reader in;
	private JsonNode data;

	public JSONLoader(Reader in) {
		this.in = in;
	}

	public JSONLoader(String content) {
		this.in = new StringReader(content);
	}

	protected JsonNode getData() throws IOException, JSONParserException {
		if (data != null)
			return data;

		data = new ObjectMapper().readTree(in);

		if (data == null)
			throw new JSONParserException("cannot parse null");

		return data;
	}
}
