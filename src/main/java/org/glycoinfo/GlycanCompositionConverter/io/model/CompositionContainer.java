package org.glycoinfo.GlycanCompositionConverter.io.model;

import java.util.Map;

public class CompositionContainer {

	private Map<String, Integer> composition;
	private String glycoCT;
	private String wurcs;

	public Map<String, Integer> getComposition() {
		return composition;
	}

	public void setComposition(Map<String, Integer> composition) {
		this.composition = composition;
	}

	public String getGlycoCT() {
		return glycoCT;
	}

	public void setGlycoCT(String glycoCT) {
		this.glycoCT = glycoCT;
	}

	public String getWurcs() {
		return wurcs;
	}

	public void setWurcs(String wurcs) {
		this.wurcs = wurcs;
	}
}
