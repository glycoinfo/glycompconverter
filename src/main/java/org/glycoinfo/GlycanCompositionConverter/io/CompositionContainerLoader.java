package org.glycoinfo.GlycanCompositionConverter.io;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.glycoinfo.GlycanCompositionConverter.io.model.CompositionContainer;

import com.fasterxml.jackson.databind.JsonNode;

public class CompositionContainerLoader {

	private JSONLoader loader;

	public CompositionContainerLoader(Reader in) {
		this.loader = new JSONLoader(in);
	}

	public CompositionContainerLoader(String in) {
		this.loader = new JSONLoader(in);
	}

	public List<CompositionContainer> getData() throws IOException, JSONParserException {
		JsonNode data = this.loader.getData();

		List<CompositionContainer> lComps = new ArrayList<>();
		for ( Iterator<JsonNode> itrNode = data.elements(); itrNode.hasNext(); ) {
			JsonNode compNode = itrNode.next();

			CompositionContainer comp = parseJsonNode(compNode);
			if ( comp == null )
				continue;
			lComps.add(comp);
		}

		return lComps;
	}

	private static CompositionContainer parseJsonNode(JsonNode node) {
		CompositionContainer comp = new CompositionContainer();
		Map<String, Integer> mapCompo = new TreeMap<>();
		Iterator<String> itrCompoName = node.fieldNames();
		while ( itrCompoName.hasNext() ) {
			String strName = itrCompoName.next();
			int count = node.get(strName).asInt();
			if ( mapCompo.containsKey(strName) ) {
				count += mapCompo.get(strName);
			}
			mapCompo.put(strName, count);
		}
		comp.setComposition(mapCompo);
		return comp;
	}

}
