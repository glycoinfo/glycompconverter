package org.glycoinfo.GlycanCompositionConverter.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.glycoinfo.GlycanCompositionConverter.structure.Composition;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MSType;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MonosaccharideDictionary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

public class CompositionImporter {

	public static final Logger logger = LoggerFactory.getLogger(CompositionImporter.class);

	public static List<Composition> parseJsonArray(String strJson) throws IOException, JSONParserException {
		JSONLoader loader = new JSONLoader(strJson);

		JsonNode node = loader.getData();
		return parseJsonArray(node);
	}

	public static List<Composition> parseJsonArray(JsonNode node) throws IOException, JSONParserException {
		if ( !MonosaccharideDictionary.hasDictionalies() ) {
			logger.error("No monosaccharide in current dictionary.");
			return null;
		}

		List<Composition> lComps = new ArrayList<>();
		for ( Iterator<JsonNode> itrNode = node.elements(); itrNode.hasNext(); ) {
			JsonNode compNode = itrNode.next();

			Composition comp = parseJsonNode(compNode);
			if ( comp == null )
				continue;
			lComps.add(comp);
		}

		return lComps;
	}

	public static Composition parseJson(String _json) throws IOException, JSONParserException {
		JSONLoader loader = new JSONLoader(_json);

		JsonNode compo = loader.getData();

		return parseJsonNode(compo);
	}

	private static Composition parseJsonNode(JsonNode node) {
		Composition comp = new Composition();
		Iterator<String> itrCompoName = node.fieldNames();
		while ( itrCompoName.hasNext() ) {
			String strName = itrCompoName.next();
			MSType type = MonosaccharideDictionary.findMonosaccharideType(strName);
			if ( type == null ) {
				logger.warn(String.format("No %s in current dictionary.", strName));
				continue;
			}
			int count = node.get(strName).asInt();
			comp.addMonosaccharide(type, count);
		}
		if ( comp.getMonosaccharideTypes().isEmpty() )
			return null;
		return comp;
	}
}
