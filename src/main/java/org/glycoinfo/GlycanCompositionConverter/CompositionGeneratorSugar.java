package org.glycoinfo.GlycanCompositionConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eurocarbdb.MolecularFramework.sugar.Anomer;
import org.eurocarbdb.MolecularFramework.sugar.BaseType;
import org.eurocarbdb.MolecularFramework.sugar.GlycoEdge;
import org.eurocarbdb.MolecularFramework.sugar.GlycoNode;
import org.eurocarbdb.MolecularFramework.sugar.GlycoconjugateException;
import org.eurocarbdb.MolecularFramework.sugar.Linkage;
import org.eurocarbdb.MolecularFramework.sugar.LinkageType;
import org.eurocarbdb.MolecularFramework.sugar.Modification;
import org.eurocarbdb.MolecularFramework.sugar.ModificationType;
import org.eurocarbdb.MolecularFramework.sugar.Monosaccharide;
import org.eurocarbdb.MolecularFramework.sugar.Substituent;
import org.eurocarbdb.MolecularFramework.sugar.Sugar;
import org.eurocarbdb.MolecularFramework.sugar.Superclass;
import org.eurocarbdb.MolecularFramework.sugar.UnderdeterminedSubTree;
import org.glycoinfo.GlycanCompositionConverter.structure.Composition;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MSType;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.SubstType;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MSGenericType.CoreModification;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MSGenericType.CoreSubstituent;

public class CompositionGeneratorSugar {

	private Sugar m_oSugar;

	public CompositionGeneratorSugar() {
		this.initialize();
	}

	private void initialize() {
		this.m_oSugar = new Sugar();
	}

	public Sugar getSugar() {
		return this.m_oSugar;
	}

	public void start(Composition a_oCompo) throws GlycoconjugateException {
		this.initialize();

		// Monosaccharides
		for ( MSType t_oMSType : a_oCompo.getMonosaccharideTypes() ) {
			// Populate monosaccharide
			Monosaccharide t_oMS = this.getMonosaccharide(t_oMSType);

			// Populate substituents and its linkages
			List<Substituent> t_aSubsts = new ArrayList<>();
			Map<Substituent, List<GlycoEdge>> t_mapSubstToEdges = new HashMap<>();
			for ( CoreSubstituent t_oCoreSubst : t_oMSType.getGenericType().getSubstituent() ) {
				Substituent t_oSubst = new Substituent(t_oCoreSubst.getType());
				t_aSubsts.add(t_oSubst);

				List<GlycoEdge> t_aEdges = new ArrayList<>();
				GlycoEdge t_oEdge = new GlycoEdge();
				t_oEdge.addGlycosidicLinkage(t_oCoreSubst.getLinkage());
				t_aEdges.add(t_oEdge);
				if ( t_oCoreSubst.getLinkageSecond() != null ) {
					t_oEdge = new GlycoEdge();
					t_oEdge.addGlycosidicLinkage(t_oCoreSubst.getLinkageSecond());
					t_aEdges.add(t_oEdge);
				}
				t_mapSubstToEdges.put(t_oSubst, t_aEdges);
			}

			// Add to sugar
			int t_nCount = a_oCompo.getNumberOfMonosaccharides(t_oMSType);
			for ( int i=0; i<t_nCount; i++ ) {
				Monosaccharide t_oMSCopy = t_oMS.copy();
				this.m_oSugar.addNode(t_oMSCopy);

				for ( Substituent t_oSubst : t_aSubsts ) {
					Substituent t_oSubstCopy = t_oSubst.copy();
					for ( GlycoEdge t_oEdge : t_mapSubstToEdges.get(t_oSubst) ) {
						GlycoEdge t_oEdgeCopy = t_oEdge.copy();
						if ( !this.m_oSugar.containsNode(t_oSubstCopy) )
							this.m_oSugar.addNode(t_oMSCopy, t_oEdgeCopy, t_oSubstCopy);
						else
							this.m_oSugar.addEdge(t_oMSCopy, t_oSubstCopy, t_oEdgeCopy);
					}
				}
			}
		}

		// Substituents 
		for ( SubstType t_oSubstType : a_oCompo.getSubstituentTypes() ) {
			// Populate edge for the connection of subtree
			Linkage t_oLin = new Linkage();
			t_oLin.setParentLinkageType(t_oSubstType.getLinkageType());
			t_oLin.setChildLinkageType(LinkageType.NONMONOSACCHARID);
			t_oLin.addParentLinkage(Linkage.UNKNOWN_POSITION);
			t_oLin.addChildLinkage(1);
			GlycoEdge t_oEdge = new GlycoEdge();
			t_oEdge.addGlycosidicLinkage(t_oLin);

			// Populate substituent
			Substituent t_oSubst = new Substituent(t_oSubstType.getSubstituentType());

			// Populate underdetermined subtree
			UnderdeterminedSubTree t_oSubTree = new UnderdeterminedSubTree();
			t_oSubTree.setConnection(t_oEdge);
			t_oSubTree.addNode(t_oSubst);

			// Duplicate subtrees 
			int t_nCount = a_oCompo.getNumberOfSubstituents(t_oSubstType);
			for ( int i=0; i<t_nCount; i++ ) {
				UnderdeterminedSubTree t_oSubTreeCopy = t_oSubTree.copy();
				this.m_oSugar.addUndeterminedSubTree(t_oSubTreeCopy);
				// Set all monosaccharides as parents
				for ( GlycoNode t_oMS : this.m_oSugar.getNodes() ) {
					if ( t_oMS instanceof Substituent )
						continue;
					this.m_oSugar.addUndeterminedSubTreeParent(t_oSubTreeCopy, t_oMS);
				}
			}
		}
	}

	private Monosaccharide getMonosaccharide(MSType a_oMSType) throws GlycoconjugateException {
		Anomer t_eAnom = Anomer.Unknown;
		Superclass t_eSuper = a_oMSType.getGenericType().getSuperclass();
		int t_iAnomPos = a_oMSType.getGenericType().getAnomericPosition();
		int t_iRingSize = a_oMSType.getGenericType().getRingSize();

		Monosaccharide t_oMS = new Monosaccharide(t_eAnom, t_eSuper);

		// Stereos
		for ( BaseType t_eBaseType : a_oMSType.getBaseTypes() ) {
			t_oMS.addBaseType(t_eBaseType);
		}

		// Ring
		t_oMS.setRing(t_iAnomPos, t_iAnomPos+t_iRingSize-2);

		// Core modificaions
		if ( t_iAnomPos != 1 )
			t_oMS.addModification(new Modification(ModificationType.KETO, t_iAnomPos));

		for ( CoreModification t_oCoreMod : a_oMSType.getGenericType().getModifications() ) {
			Modification t_oMod = new Modification(t_oCoreMod.getType(), t_oCoreMod.getPosition());
			t_oMS.addModification(t_oMod);
		}

		return t_oMS;
	}
}
