package org.glycoinfo.GlycanCompositionConverter.structure;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.glycoinfo.GlycanCompositionConverter.structure.residue.MSType;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.ResidueType;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.SubstType;

public class Composition {

	private Map<MSType, Integer> m_mapMSToCount;
	private Map<SubstType, Integer> m_mapSubstToCount;

	public Composition() {
		this.m_mapMSToCount = new TreeMap<>();
		this.m_mapSubstToCount = new TreeMap<>();
	}

	public void addResidue(ResidueType a_oType) {
		this.addResidue(a_oType, 1);
	}

	public void addResidue(ResidueType a_oType, int a_iCount) {
		if ( a_oType instanceof MSType )
			this.addMonosaccharide((MSType)a_oType, a_iCount);
		else if ( a_oType instanceof SubstType )
			this.addSubstituent((SubstType)a_oType, a_iCount);
	}

	public void addMonosaccharide(MSType a_oMSType) {
		this.addMonosaccharide(a_oMSType, 1);
	}

	public void addMonosaccharide(MSType a_oMSType, int a_iCount) {
		if ( a_oMSType == null )
			return;

		if (!this.m_mapMSToCount.containsKey(a_oMSType))
			this.m_mapMSToCount.put(a_oMSType, 0);
		int t_iCount = this.m_mapMSToCount.get(a_oMSType);
		t_iCount += a_iCount;
		if (t_iCount == 0) {
			this.m_mapMSToCount.remove(a_oMSType);
			return;
		}
		this.m_mapMSToCount.put(a_oMSType, t_iCount);
	}

	public Set<MSType> getMonosaccharideTypes() {
		Set<MSType> t_aMSTypes = new TreeSet<>();
		for (MSType type : this.m_mapMSToCount.keySet())
			t_aMSTypes.add(type);
		return t_aMSTypes;
	}

	/**
	 * Returns the number of the given monosaccharide.
	 * @param a_oMSType MSType
	 */
	public int getNumberOfMonosaccharides(MSType a_oMSType) {
		if (!this.m_mapMSToCount.containsKey(a_oMSType))
			return 0;
		return this.m_mapMSToCount.get(a_oMSType);
	}

	public void addSubstituent(SubstType a_oSubstType) {
		this.addSubstituent(a_oSubstType, 1);
	}

	public void addSubstituent(SubstType a_oSubstType, int a_iCount) {
		if ( a_oSubstType == null )
			return;

		if (!this.m_mapSubstToCount.containsKey(a_oSubstType))
			this.m_mapSubstToCount.put(a_oSubstType, 0);
		int t_iCount = this.m_mapSubstToCount.get(a_oSubstType);
		t_iCount += a_iCount;
		if (t_iCount == 0)
			this.m_mapSubstToCount.remove(a_oSubstType);
		this.m_mapSubstToCount.put(a_oSubstType, t_iCount);
	}

	public Set<SubstType> getSubstituentTypes() {
		Set<SubstType> t_aSubstTypes = new TreeSet<>();
		for (SubstType type : this.m_mapSubstToCount.keySet())
			t_aSubstTypes.add(type);
		return t_aSubstTypes;
	}

	/**
	 * Returns the number of the given substituent or {@code -1} if no residue in
	 * this structure.
	 * 
	 * @param a_oSubstType SubstType
	 */
	public int getNumberOfSubstituents(SubstType a_oSubstType) {
		if (!this.m_mapSubstToCount.containsKey(a_oSubstType))
			return 0;
		return this.m_mapSubstToCount.get(a_oSubstType);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Composition))
			return false;
		Composition comp = (Composition) obj;

		if (this.m_mapMSToCount.size() != comp.m_mapMSToCount.size())
			return false;
		for (MSType res : this.m_mapMSToCount.keySet()) {
			if (!comp.m_mapMSToCount.containsKey(res))
				return false;
			if (comp.m_mapMSToCount.get(res) != this.m_mapMSToCount.get(res))
				return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (MSType type : this.m_mapMSToCount.keySet()) {
			if (sb.length() != 0)
				sb.append("|");
			int n = this.m_mapMSToCount.get(type);
			sb.append(type.toString());
			sb.append(":");
			sb.append(n);
		}
		for (SubstType type : this.m_mapSubstToCount.keySet()) {
			if (sb.length() != 0)
				sb.append("|");
			int n = this.m_mapSubstToCount.get(type);
			sb.append(type.toString());
			sb.append(":");
			sb.append(n);
		}
		return sb.toString();
	}

}
