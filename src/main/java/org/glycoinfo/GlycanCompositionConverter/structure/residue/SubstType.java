package org.glycoinfo.GlycanCompositionConverter.structure.residue;

import java.util.List;

import org.eurocarbdb.MolecularFramework.sugar.LinkageType;
import org.eurocarbdb.MolecularFramework.sugar.SubstituentType;

public class SubstType extends ResidueType {

	private SubstituentType m_type;
	private LinkageType m_linkType;

	protected SubstType(String a_strName, List<String> a_lSynonyms,
			SubstituentType a_type, LinkageType a_linkType, String a_strDesc) {
		super(a_strName, a_lSynonyms, a_strDesc);
		this.m_type = a_type;
		this.m_linkType = a_linkType;
	}

	public SubstituentType getSubstituentType() {
		return this.m_type;
	}

	public LinkageType getLinkageType() {
		return this.m_linkType;
	}

}

