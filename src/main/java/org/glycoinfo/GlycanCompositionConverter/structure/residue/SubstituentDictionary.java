package org.glycoinfo.GlycanCompositionConverter.structure.residue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.eurocarbdb.MolecularFramework.sugar.GlycoconjugateException;
import org.eurocarbdb.MolecularFramework.sugar.LinkageType;
import org.eurocarbdb.MolecularFramework.sugar.SubstituentType;
import org.glycoinfo.GlycanCompositionConverter.utils.DictionaryException;
import org.glycoinfo.GlycanCompositionConverter.utils.DictionaryParserException;
import org.glycoinfo.GlycanCompositionConverter.utils.TextFileUtils;
import org.glycoinfo.GlycanCompositionConverter.utils.TextUtils;

public class SubstituentDictionary {
	private static final String DICT_FILE = "/conf/subst/subst_type";

	private static String strCurrentSubstFile;

	private static HashMap<String, SubstType> mapNameToSubsts;

	static {
		initialize();
	}

	/**
	 * Initializes all dictionaries
	 */
	public static void initialize() {
		mapNameToSubsts = new HashMap<>();
		strCurrentSubstFile = null;
	}

	/**
	 * Loads default dictionaries for substituents.
	 * 
	 * @see #loadSubstituentTypes(String, String)
	 */
	public static void loadDefaultDictionaries() {
		SubstituentDictionary.loadSubstituentTypes(DICT_FILE);
	}

	public static boolean hasDictionalies() {
		if ( mapNameToSubsts.isEmpty())
			return false;
		return true;
	}

	/**
	 * Returns all substituents.
	 */
	public static Collection<SubstType> getSubstituents() {
		Set<SubstType> setTypes = new TreeSet<>();
		setTypes.addAll(mapNameToSubsts.values());
		return setTypes;
	}

	/**
	 * Returns the monosaccharide type with the given name
	 * 
	 * @see SubstType
	 * @throws DictionaryException if there is no substituent type with the given
	 *                             name
	 */
	public static SubstType getSubstituentType(String strType) throws DictionaryException {
		SubstType ret = findSubstituentType(strType);
		if (ret == null)
			throw new DictionaryException("Invalid substituent type: <" + strType + ">");
		return ret;
	}

	/**
	 * Returns the substituent type with the given name or {@code null} otherwise
	 * 
	 * @see SubstType
	 */
	public static SubstType findSubstituentType(String strType) {
		SubstType ret = mapNameToSubsts.get(strType.toLowerCase());
		return ret;
	}


	public static void loadSubstituentTypes(String a_strSubstDictFile) {
		try {
			strCurrentSubstFile = a_strSubstDictFile;
			// Load generic monosaccharide dictionary
			List<SubstType> t_lSubsts = loadSubstituentTypeDictionary(a_strSubstDictFile);
			for (SubstType type : t_lSubsts) {
				mapNameToSubsts.put(type.getName().toLowerCase(), type);
				for (String s : type.getSynonyms())
					mapNameToSubsts.put(s.toLowerCase(), type);
			}

		} catch (DictionaryException e) {
			e.printStackTrace();
			mapNameToSubsts.clear();
		}
	}

	private static List<SubstType> loadSubstituentTypeDictionary(String filename)
			throws DictionaryException {
		try {
			List<SubstType> lTypes = new ArrayList<>();
			List<String> lines = TextFileUtils.getLines(filename);
			if (lines == null)
				return lTypes;
			for (String line : lines) {
				SubstType type = parseSubstType(line);
				// TODO: need validation for substituent type
				lTypes.add(type);
			}

			return lTypes;
		} catch (IOException e) {
			throw new DictionaryException("An error is occured in loading subst type dictionary:\n" + e.getMessage());
		}
	}

	private static SubstType parseSubstType(String line) throws DictionaryException {
		List<String> tokens = TextUtils.tokenize(line, "\t");

		if (tokens.size() != 5)
			throw new DictionaryParserException("Invalid line for substituent type: " + line);

		String name = tokens.get(0);
		List<String> synonyms = TextUtils.parseStringArray(tokens.get(1));
		SubstituentType type = SubstituentType.forName(tokens.get(2));
		if (type == null)
			throw new DictionaryException("Invalid substituent type: <"+tokens.get(2)+">");
		String strLinkType = tokens.get(3);
		if ( strLinkType.length() > 1 )
			throw new DictionaryException("Invalid linkage type: <"+strLinkType+">");
		LinkageType linkType;
		try {
			linkType = LinkageType.forName(strLinkType.charAt(0));
		} catch (GlycoconjugateException e) {
			throw new DictionaryException("Invalid linkage type: <"+strLinkType+">");
		}
		String desc = tokens.get(4);

		return new SubstType(name, synonyms, type, linkType, desc);
	}


}
