package org.glycoinfo.GlycanCompositionConverter.structure.residue;

import java.util.ArrayList;
import java.util.List;

public abstract class ResidueType implements Comparable<ResidueType> {

	private String m_strName;
	private List<String> m_lSynonyms;
	private String m_strDesc;

	public ResidueType(String a_strName, List<String> a_lSynonyms, String a_strDesc) {
		this.m_strName = a_strName;
		this.m_lSynonyms = a_lSynonyms;
		this.m_strDesc = a_strDesc;
	}

	public String getName() {
		return this.m_strName;
	}

	public List<String> getSynonyms() {
		// Copy list
		List<String> t_aCopy = new ArrayList<>();
		for ( String t_strSynonym : this.m_lSynonyms )
			t_aCopy.add(t_strSynonym);
		return t_aCopy;
	}

	public String getDescription() {
		return this.m_strDesc;
	}

	@Override
	public String toString() {
		return this.m_strName;
	}

	public int compareTo(ResidueType objType) {
		return this.m_strName.compareTo(objType.m_strName);
	}

}
