package org.glycoinfo.GlycanCompositionConverter.structure.residue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.eurocarbdb.MolecularFramework.sugar.BaseType;
import org.eurocarbdb.MolecularFramework.sugar.GlycoconjugateException;
import org.eurocarbdb.MolecularFramework.sugar.LinkageType;
import org.eurocarbdb.MolecularFramework.sugar.ModificationType;
import org.eurocarbdb.MolecularFramework.sugar.SubstituentType;
import org.eurocarbdb.MolecularFramework.sugar.Superclass;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MSGenericType.CoreModification;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MSGenericType.CoreSubstituent;
import org.glycoinfo.GlycanCompositionConverter.utils.DictionaryException;
import org.glycoinfo.GlycanCompositionConverter.utils.DictionaryParserException;
import org.glycoinfo.GlycanCompositionConverter.utils.DictionaryParserUtils;
import org.glycoinfo.GlycanCompositionConverter.utils.TextFileUtils;
import org.glycoinfo.GlycanCompositionConverter.utils.TextUtils;

public class MonosaccharideDictionary {

	private static final String GENERIC_DICT_FILE = "/conf/ms/ms_generic_type";
	private static final String MS_DICT_FILE = "/conf/ms/ms_type_snfg";

	private static String strCurrentGenericFile;
	private static String strCurrentMonosaccharideFile;

	private static HashMap<String, MSType> mapNameToMSs;
	private static HashMap<String, MSGenericType> mapNameToGenericMSs;

	static {
		initialize();
	}

	/**
	 * Initializes all dictionaries
	 */
	public static void initialize() {
		mapNameToMSs = new HashMap<>();
		mapNameToGenericMSs = new HashMap<>();
		strCurrentGenericFile = null;
		strCurrentMonosaccharideFile = null;
	}

	/**
	 * Loads default dictionaries for monosaccharides.
	 * 
	 * @see #loadMonosaccharideTypes(String, String)
	 */
	public static void loadDefaultDictionaries() {
		MonosaccharideDictionary.loadMonosaccharideTypes(GENERIC_DICT_FILE, MS_DICT_FILE);
	}

	public static boolean hasDictionalies() {
		if ( mapNameToGenericMSs.isEmpty())
			return false;
		if ( mapNameToMSs.isEmpty() )
			return false;
		return true;
	}

	public static void printCurrentMonosaccharideDictionaries() {
		if ( strCurrentGenericFile == null ) {
			System.out.println("No generic dictionary is loaded.");
		} else {
			System.out.println("Generic monosaccharide dictionary:");
			try {
				for ( String line : TextFileUtils.getLines(strCurrentGenericFile) )
					System.out.println(line);
			} catch (IOException e) {
				System.out.println("Error in loading file: "+strCurrentGenericFile);
			}
		}
		System.out.println();

		if ( strCurrentMonosaccharideFile == null ) {
			System.out.println("No monosaccharide dictionary is loaded.");
		} else {
			System.out.println("Monosaccharide dictionary:");
			try {
				for ( String line : TextFileUtils.getLines(strCurrentMonosaccharideFile) )
					System.out.println(line);
			} catch (IOException e) {
				System.out.println("Error in loading file: "+strCurrentMonosaccharideFile);
			}
		}
		System.out.println();
	}

	/**
	 * Returns all monosaccharides.
	 */
	public static Collection<MSType> getMonosaccharides() {
		Set<MSType> setTypes = new TreeSet<>();
		setTypes.addAll(mapNameToMSs.values());
		return setTypes;
	}

	/**
	 * Returns the monosaccharide type with the given name
	 * 
	 * @see MSType
	 * @throws DictionaryException if there is no monosaccharide type with the given
	 *                             name
	 */
	public static MSType getMonosaccharideType(String strType) throws DictionaryException {
		MSType ret = findMonosaccharideType(strType);
		if (ret == null)
			throw new DictionaryException("Invalid monosaccharide type: <" + strType + ">");
		return ret;
	}

	/**
	 * Returns the monosaccharide type with the given name or {@code null} otherwise
	 * 
	 * @see MonosaccharideType
	 */
	public static MSType findMonosaccharideType(String strType) {
		MSType ret = mapNameToMSs.get(strType.toLowerCase());
		return ret;
	}

	/**
	 * Loads MSGenericTypes and MSTypes listed in the given file into monosaccharide
	 * dictionary. If generic monosaccharides in the generic dictionary file have
	 * substituents and/or core modifications, the name of substituents and
	 * modifications must be specified in SubstituentType and ModificationType
	 * classes, respectively. The generic monosaccharides of a monosaccharide in the
	 * monosaccharide dictionary file must be specified in the generic dictionary file.
	 * 
	 * @param a_strGenericDictFile the file name containing generic monosaccharides
	 * @param a_strMSDictFile      the file name containing monosaccharides
	 */
	public static void loadMonosaccharideTypes(String a_strGenericDictFile, String a_strMSDictFile) {
		try {
			strCurrentGenericFile = a_strGenericDictFile;
			// Load generic monosaccharide dictionary
			List<MSGenericType> t_aGenericMSs = loadGenericMonosaccharideTypeDictionary(a_strGenericDictFile);
			for (MSGenericType type : t_aGenericMSs) {
				mapNameToGenericMSs.put(type.getName().toLowerCase(), type);
				for (String s : type.getSynonyms())
					mapNameToGenericMSs.put(s.toLowerCase(), type);
			}

			strCurrentMonosaccharideFile = a_strMSDictFile;
			// Load monosaccharide dictionary
			List<MSType> t_aMSs = loadMonosaccharideTypeDictionary(a_strMSDictFile);
			for ( MSType type : t_aMSs ) {
				mapNameToMSs.put(type.getName().toLowerCase(), type);
				for (String s : type.getSynonyms()) {
					mapNameToMSs.put(s.toLowerCase(), type);
				}
			}
		} catch (DictionaryException e) {
			e.printStackTrace();
			mapNameToGenericMSs.clear();
		}
	}

	private static List<MSGenericType> loadGenericMonosaccharideTypeDictionary(String filename)
			throws DictionaryException {
		try {
			List<MSGenericType> lTypes = new ArrayList<>();
			List<String> lines = TextFileUtils.getLines(filename);
			if (lines == null)
				return lTypes;
			for (String line : lines) {
				MSGenericType type = parseGenericMonosaccharideType(line);
				// TODO: need validation for monosaccharide type
				lTypes.add(type);
			}

			return lTypes;
		} catch (IOException e) {
			throw new DictionaryException("An error is occured in loading MS type dictionary:\n" + e.getMessage());
		}
	}

	private static MSGenericType parseGenericMonosaccharideType(String line) throws DictionaryException {
		List<String> tokens = TextUtils.tokenize(line, "\t");

		if (tokens.size() != 8)
			throw new DictionaryParserException("Invalid line for generic monosaccharide type: " + line);

		String name = tokens.get(0);
		List<String> synonyms = TextUtils.parseStringArray(tokens.get(1));
		int iCLength = TextUtils.parseInteger(tokens.get(2));
		Superclass t_eSuper = Superclass.forCAtomCount(iCLength);
		int iAnomPos = TextUtils.parseInteger(tokens.get(3));
		int iRingSize = TextUtils.parseInteger(tokens.get(4));
		List<CoreModification> lMods = parseModifications(tokens.get(5));
		List<CoreSubstituent> lSubsts = parseSubstituents(tokens.get(6));
		String desc = tokens.get(7);

		return new MSGenericType(name, synonyms, t_eSuper, iAnomPos, iRingSize, lMods, lSubsts, desc);
	}

	private static List<CoreModification> parseModifications(String strMods) throws DictionaryParserException {
		List<CoreModification> lMods = new ArrayList<>();
		if (strMods == null || strMods.equals("-"))
			return lMods;

		String[] tokens = strMods.split(",");
		for (String token : tokens) {
			String[] strMod = token.split(":");
			int pos = TextUtils.parseInteger(strMod[0]);
			ModificationType type = null;
			try {
				type = ModificationType.forName(strMod[1]);
			} catch (GlycoconjugateException e) {
				e.printStackTrace();
			}
			if (type == null)
				throw new DictionaryParserException("The modification type is not available.");
			lMods.add(new CoreModification(type, pos));
		}

		return lMods;
	}

	private static List<CoreSubstituent> parseSubstituents(String a_strSubsts) throws DictionaryException {
		List<CoreSubstituent> t_aSubsts = new ArrayList<>();
		if (a_strSubsts == null || a_strSubsts.equals("-"))
			return t_aSubsts;

			String[] tokens = a_strSubsts.split(",");
			for (String token : tokens) {
				String[] strMod = token.split(":");

				SubstituentType subType = SubstituentType.forName(strMod[1]);
				if ( subType == null )
					throw new DictionaryParserException("Invalid substituent name: <"+strMod[1]+">");

				try {
					if (!strMod[0].contains("-")) {
						List<String> aLins = DictionaryParserUtils.parseLinkage(strMod[0]);
						int pos = Integer.valueOf(aLins.get(0));
						LinkageType linType = LinkageType.forName(aLins.get(1).charAt(0));

						t_aSubsts.add(new CoreSubstituent(subType, pos, linType));
						continue;
					}

					String[] strLins = strMod[0].split("-");
					// First linkage
					List<String> aLins = DictionaryParserUtils.parseLinkage(strLins[0]);
					int pos1 = Integer.valueOf(aLins.get(0));
					LinkageType linType1 = LinkageType.forName(aLins.get(1).charAt(0));
					// Second linkage
					aLins = DictionaryParserUtils.parseLinkage(strLins[1]);
					int pos2 = Integer.valueOf(aLins.get(1));
					LinkageType linType2 = LinkageType.forName(aLins.get(1).charAt(0));

					t_aSubsts.add(new CoreSubstituent(subType, pos1, linType1, pos2, linType2));
				} catch (GlycoconjugateException e) {
					throw new DictionaryParserException("Invalid linkage: <"+strMod[0]+">");
				}
			}

		return t_aSubsts;
	}

	private static List<MSType> loadMonosaccharideTypeDictionary(String filename)
			throws DictionaryException {
		try {
			List<MSType> lTypes = new ArrayList<>();
			List<String> lines = TextFileUtils.getLines(filename);
			if (lines == null)
				return lTypes;
			for (String line : lines) {
				MSType type = parseMonosaccharideType(line);
				// TODO: need validation for monosaccharide type
				lTypes.add(type);
			}

			return lTypes;
		} catch (IOException e) {
			throw new DictionaryException("An error is occured in loading MS type dictionary:\n" + e.getMessage());
		}
	}

	private static MSType parseMonosaccharideType(String line) throws DictionaryException {
		List<String> tokens = TextUtils.tokenize(line, "\t");

		if (tokens.size() != 5)
			throw new DictionaryParserException("Invalid line for monosaccharide type: " + line);

		String name = tokens.get(0);
		List<String> synonyms = TextUtils.parseStringArray(tokens.get(1));
		String generic = tokens.get(2).toLowerCase();
		if ( !mapNameToGenericMSs.containsKey(generic) )
			throw new DictionaryException("Invalid generic monosaccharide type: <"+tokens.get(2)+">" );
		MSGenericType t_oMSGenericType = mapNameToGenericMSs.get(generic);
		List<BaseType> t_aBaseTypes = parseBaseTypes(tokens.get(3));
		String desc = tokens.get(4);

		return new MSType(name, synonyms, t_oMSGenericType, t_aBaseTypes, desc);
	}

	private static List<BaseType> parseBaseTypes(String a_strBaseTypes) throws DictionaryParserException {
		List<BaseType> t_aBaseTypes = new ArrayList<>();
		if (a_strBaseTypes == null)
			return t_aBaseTypes;

		List<String> tokens = TextUtils.parseStringArray(a_strBaseTypes);
		for ( String token : tokens ) {
			try {
				t_aBaseTypes.add(BaseType.forName(token));
			} catch (GlycoconjugateException e) {
				throw new DictionaryParserException("Invalid basetype: <"+token+">");
			}
		}
		return t_aBaseTypes;
	}
}
