package org.glycoinfo.GlycanCompositionConverter.structure.residue;

import java.util.ArrayList;
import java.util.List;

import org.eurocarbdb.MolecularFramework.sugar.BaseType;

public class MSType extends ResidueType {

	private MSGenericType m_oGenericMSType;
	private List<BaseType> m_aBaseTypes;

	protected MSType(String a_strName, List<String> a_lSynonyms,
			MSGenericType a_oSuper, List<BaseType> a_aBaseTypes, String a_strDesc) {
		super(a_strName, a_lSynonyms, a_strDesc);
		this.m_aBaseTypes = a_aBaseTypes;
		this.m_oGenericMSType = a_oSuper;
	}

	public MSGenericType getGenericType() {
		return this.m_oGenericMSType;
	}

	public List<BaseType> getBaseTypes() {
		// Copy list
		List<BaseType> t_aCopy = new ArrayList<>();
		for ( BaseType t_ebaseType : this.m_aBaseTypes )
			t_aCopy.add(t_ebaseType);
		return t_aCopy;
	}

}
