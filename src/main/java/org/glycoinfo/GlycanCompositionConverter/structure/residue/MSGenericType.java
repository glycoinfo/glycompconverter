package org.glycoinfo.GlycanCompositionConverter.structure.residue;

import java.util.List;

import org.eurocarbdb.MolecularFramework.sugar.GlycoconjugateException;
import org.eurocarbdb.MolecularFramework.sugar.Linkage;
import org.eurocarbdb.MolecularFramework.sugar.LinkageType;
import org.eurocarbdb.MolecularFramework.sugar.ModificationType;
import org.eurocarbdb.MolecularFramework.sugar.SubstituentType;
import org.eurocarbdb.MolecularFramework.sugar.Superclass;

public class MSGenericType extends ResidueType {

	private Superclass m_eSuper;
	private int m_iAnomPos;
	private int m_iRingSize;

	private List<CoreModification> m_aMods;
	private List<CoreSubstituent> m_aSubsts;

	/**
	 * An inner class for core modifications.
	 */
	public static class CoreModification {
		private ModificationType m_eType;
		private int m_iPosition;

		/**
		 * @param a_eType ModificationType
		 * @param a_iPos The number of modification position. A negative value for unknown position.
		 */
		protected CoreModification(ModificationType a_eType, int a_iPos) {
			this.m_eType = a_eType;
			this.m_iPosition = a_iPos;
		}

		public int getPosition() {
			return this.m_iPosition;
		}

		public ModificationType getType() {
			return this.m_eType;
		}

		public String toString() {
			return this.m_iPosition + ":" + this.m_eType.getName();
		}
	}

	/**
	 * An inner class for core substituents.
	 */
	public static class CoreSubstituent {
		private SubstituentType m_eType;
		private int m_iPos1;
		private LinkageType m_eLinType1;
		private int m_iPos2;
		private LinkageType m_eLinType2;

		/**
		 * @param a_eType SubstituentType
		 * @param a_eLinType LinkageType on parent side
		 * @param a_iPos The number of modification position. A negative value for unknown position.
		 * @throws GlycoconjugateException 
		 */
		public CoreSubstituent(SubstituentType a_eType,
				int a_iPos, LinkageType a_eLinType ) {
			this.m_eType = a_eType;

			this.m_iPos1 = a_iPos;
			this.m_eLinType1 = a_eLinType;
			this.m_iPos2 = -1;
			this.m_eLinType2 = null;
		}

		/**
		 * @param a_eType SubstituentType
		 * @param a_eLinType LinkageType on parent side
		 * @param a_iPos The number of modification position. A negative value for unknown position.
		 * @throws GlycoconjugateException 
		 */
		public CoreSubstituent(SubstituentType a_eType,
				int a_iPos1, LinkageType a_eLinType1,
				int a_iPos2, LinkageType a_eLinType2) {
			this.m_eType = a_eType;

			this.m_iPos1 = a_iPos1;
			this.m_eLinType1 = a_eLinType1;
			this.m_iPos2 = a_iPos2;
			this.m_eLinType2 = a_eLinType2;
		}

		public Linkage getLinkage() {
			Linkage t_oLin = new Linkage();
			try {
				t_oLin.setParentLinkageType(this.m_eLinType1);
				// Always 'n' for child linkage type of a substituent
				t_oLin.setChildLinkageType(LinkageType.NONMONOSACCHARID);
			} catch (GlycoconjugateException e) {
				// Happens only when linkage type is null
				return null;
			}

			t_oLin.addParentLinkage(this.m_iPos1);
			// Always 1 for child linkage position of a substituent
			t_oLin.addChildLinkage(1);

			return t_oLin;
		}

		public Linkage getLinkageSecond() {
			Linkage t_oLin = new Linkage();
			try {
				t_oLin.setParentLinkageType(this.m_eLinType2);
				// Always 'n' for child linkage type of a substituent
				t_oLin.setChildLinkageType(LinkageType.NONMONOSACCHARID);
			} catch (GlycoconjugateException e) {
				// Happens only when linkage type is null
				return null;
			}

			t_oLin.addParentLinkage(this.m_iPos2);
			// Always 1 for child linkage position of a substituent
			t_oLin.addChildLinkage(1);

			return t_oLin;
		}

		public SubstituentType getType() {
			return this.m_eType;
		}

		public String toString() {
			String str = ""+this.m_iPos1+this.m_eLinType1.getType();
			if ( this.m_eLinType2 != null )
				str += "-"+this.m_iPos2+this.m_eLinType2.getType();
			return str + ":" + this.m_eType.getName();
		}
	}

	public MSGenericType(String a_strName, List<String> a_lSynonyms,
			Superclass a_eSuper, int a_iAnomPos, int a_iRingSize,
			List<CoreModification> a_aMods, List<CoreSubstituent> a_aSubsts,
			String a_strDesc) {
		super(a_strName, a_lSynonyms, a_strDesc);
		this.m_eSuper = a_eSuper;
		this.m_iAnomPos = a_iAnomPos;
		this.m_iRingSize = a_iRingSize;
		this.m_aMods = a_aMods;
		this.m_aSubsts = a_aSubsts;
	}

	public Superclass getSuperclass() {
		return this.m_eSuper;
	}

	public int getAnomericPosition() {
		return this.m_iAnomPos;
	}

	public int getRingSize() {
		return this.m_iRingSize;
	}

	public List<CoreModification> getModifications() {
		return this.m_aMods;
	}

	public List<CoreSubstituent> getSubstituent() {
		return this.m_aSubsts;
	}
}
