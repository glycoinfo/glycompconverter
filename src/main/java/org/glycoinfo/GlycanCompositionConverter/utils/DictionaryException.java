package org.glycoinfo.GlycanCompositionConverter.utils;

public class DictionaryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2769682702749570611L;

	public DictionaryException(String string) {
		super(string);
	}
}
