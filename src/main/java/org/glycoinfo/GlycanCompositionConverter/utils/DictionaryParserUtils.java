package org.glycoinfo.GlycanCompositionConverter.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eurocarbdb.MolecularFramework.sugar.LinkageType;

public class DictionaryParserUtils {

	private static Pattern patLinkage;

	static {
		String t_strLinTypes = "";
		for ( LinkageType type : LinkageType.values() )
			t_strLinTypes += type.getType();
		patLinkage = Pattern.compile("([0-9]+)(["+t_strLinTypes+"])");
	}

	public static List<String> parseLinkage(String a_strLinkage) throws DictionaryParserException {
		Matcher mat = patLinkage.matcher(a_strLinkage);
		if ( !mat.matches() || mat.groupCount() != 2 || mat.group(2).isEmpty() )
			throw new DictionaryParserException("Illegal linkage format: "+a_strLinkage);

		List<String> t_aPosAndType = new ArrayList<>();
		t_aPosAndType.add(mat.group(1));
		t_aPosAndType.add(mat.group(2));
		return t_aPosAndType;
	}

}
