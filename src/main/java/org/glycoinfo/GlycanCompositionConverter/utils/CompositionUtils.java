package org.glycoinfo.GlycanCompositionConverter.utils;

import org.glycoinfo.GlycanCompositionConverter.structure.Composition;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.MonosaccharideDictionary;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.ResidueType;
import org.glycoinfo.GlycanCompositionConverter.structure.residue.SubstituentDictionary;

public class CompositionUtils {

	/**
	 * Parse the given String format of Composition.
	 * The residues must be separated with "|" each other if two ore more
	 * and each residue must have its type and count separated with ":".
	 * @param strCompo String format of Composition to parse
	 * @return Composition parsed from the given String
	 * @throws DictionaryException Thrown when a residue type is not found from current dictionary
	 * @throws CompositionParseException Thrown when the given String format is illegal
	 */
	public static Composition parse(String strCompo) throws DictionaryException, CompositionParseException {
		if ( strCompo == null )
			return null;

		Composition compo = new Composition();

		String[] lRess = strCompo.split("\\|");
		for ( String strResCount : lRess ) {
			String[] res = strResCount.split(":");

			if ( res.length != 2 )
				throw new CompositionParseException("Illegal residue format <"+strResCount+">: "
						+ "The residue must have the type name and count separated with \":\"");

			String strType = res[0];
			int count = 0;
			try {
				count = Integer.valueOf(res[1]);
				if ( count < 0 )
					throw new CompositionParseException("The residue count must be a positive number.");
			} catch ( NumberFormatException e ) {
				throw new CompositionParseException("The residue count must be a number.");
			}
			ResidueType type = parseResidue(strType);
			compo.addResidue(type, count);
		}

		return compo;
	}

	public static ResidueType parseResidue(String strType) throws DictionaryException {
		if ( !MonosaccharideDictionary.hasDictionalies() )
			MonosaccharideDictionary.loadDefaultDictionaries();

		// Search monosaccharide first
		ResidueType type = MonosaccharideDictionary.findMonosaccharideType(strType);
		if ( type != null )
			return type;

		if ( !SubstituentDictionary.hasDictionalies() )
			SubstituentDictionary.loadDefaultDictionaries();

		// Search substituent
		type = SubstituentDictionary.findSubstituentType(strType);
		if ( type != null )
			return type;

		// TODO: Search core structure

		throw new DictionaryException("Invalid residue type: <" + strType + ">");
	}

	
}
