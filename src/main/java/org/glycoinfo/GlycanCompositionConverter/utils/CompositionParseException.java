package org.glycoinfo.GlycanCompositionConverter.utils;

public class CompositionParseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4077275839192951325L;

	public CompositionParseException(String string) {
		super(string);
	}
}
